async function getData() {
    try {
        const data = await fetch('https://ajax.test-danit.com/api/swapi/films');
        const films = await data.json();
        return films;

    } catch (err) {
        console.log(err);
    }
}
const data = await getData();

export const filmsData = data.map(film => {
    const { episodeId, name, openingCrawl, characters } = film;
    return { episodeId, "Назва": name, "Опис": openingCrawl, characters }
})


