export async function loadCaracters(data, episodeID, episodeList) {
    try {
        const promiseCaracters = data[episodeID - 1].characters.map(async charLink => {
            const res = await fetch(charLink);
            const char = await res.json();
            return char.name;
        })
        const charactersList = await Promise.all(promiseCaracters);
        const loader = episodeList.querySelector('.loader')
        setTimeout(() => {
            loader.remove();
            episodeList.insertAdjacentHTML('beforeend', `<li><b>Персонажі</b>: ${charactersList.join(', ')}</li>`)
        }, 2000)

    } catch (err) {
        console.log(err);
    }
}