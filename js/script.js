
import { filmsData } from "./fetchData.js";
import { createList } from "./createList.js";

const container = document.querySelector('#root');
filmsData.sort((a, b) => a.episodeId - b.episodeId);

createList(filmsData, container);