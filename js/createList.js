import { createLoader } from "./createLoader.js";
import { loadCaracters } from "./fetchCharacters.js";

export const createList = (data, parent = document.body) => {
    const list = document.createElement('ul');
    list.classList.add('list');

    data.forEach(episode => {
        const film = document.createElement('li');
        const filmDescr = document.createElement('ul');
        film.className = "film";

        Object.entries(episode).slice(1, -1).forEach(el => {
            filmDescr.insertAdjacentHTML('beforeend', `<li><b>${el[0]}</b>: "${el[1]}"</li>`)
        })

        film.innerHTML = `EPISODE ${episode.episodeId}:`;
        film.append(filmDescr);
        list.append(film);
        film.insertAdjacentHTML('beforeend', `<button class='btn'>Завантажити список персонажів</button>`);

        const btn = film.querySelector('.btn');
        btn.addEventListener('click', () => {
            loadCaracters(data, episode.episodeId, filmDescr);
            btn.remove();
            const episodeLoader = createLoader();
            filmDescr.append(episodeLoader);
        });
    })
    parent.append(list);
} 